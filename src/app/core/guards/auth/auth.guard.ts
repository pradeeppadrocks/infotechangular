import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService, TitleService } from '../../services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private _SessionService:SessionService,
    private _Router:Router,
    private titleUrl: TitleService,
  ){}

  setPageName(currentUrl: string) {
    this.titleUrl.setPageTitle(currentUrl)
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      this.setPageName(next.data.title);

    if (this._SessionService.checkLogin()) {

      return true;
    }
    this._Router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}

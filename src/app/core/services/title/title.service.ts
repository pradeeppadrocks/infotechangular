import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable()
export class TitleService {

  constructor(private title: Title) { }
  getPageTitle() {
    let titleUrl = this.title.getTitle()
    if (titleUrl) {
      return titleUrl;
    } else {
      return null;
    }
  }

  /**
   * Set page title to title tag
   * @param urlTitle 
   */
  setPageTitle(urlTitle: string) {
    this.title.setTitle(`${urlTitle} - Pradeep Shetty`);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth/auth.guard';
import { ProductComponent } from './product/product.component';

const routes: Routes = [

  {
    path:'Product',
    component:ProductComponent,
     canActivate:[AuthGuard],
     data: {
      title: "Product"
    }
  },

  {
    path: '',
    redirectTo: ''
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppinessRoutingModule { }

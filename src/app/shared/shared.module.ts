import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [],
  imports: [CommonModule, DataTablesModule ],
  exports: [ ReactiveFormsModule, FormsModule, DataTablesModule ]
})
export class SharedModule { }
